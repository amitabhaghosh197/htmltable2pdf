# Library

+ [jsPDF Auto Table](https://github.com/simonbengtsson/jsPDF-AutoTable)
+ [Reff: How to change the color of cell borders](https://github.com/simonbengtsson/jsPDF-AutoTable/issues/125)
+ [Reff : JSPdf autotable header border](https://stackoverflow.com/questions/41348904/jspdf-autotable-header-border)